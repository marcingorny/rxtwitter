//
//  RXTwitterController.h
//  iBooktonic
//
//  Created by Marcin on 7/22/13.
//  Copyright (c) 2013 Marcin Górny. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RXTwitterDelegate

-(void)loggedIn:(NSArray *)response;

@end
@interface RXTwitterController : NSObject <UIActionSheetDelegate>

@property (nonatomic, strong) id<RXTwitterDelegate> delegate;

-(void)connectWithTwitterWithView:(UIView *)view;
-(id)initWithDelegate:(id<RXTwitterDelegate>)delegate;
- (void)refreshTwitterAccounts;
@end
