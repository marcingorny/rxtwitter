//
//  RXTwitterController.m
//  iBooktonic
//
//  Created by Marcin on 7/22/13.
//  Copyright (c) 2013 Marcin Górny. All rights reserved.
//

#import <Accounts/Accounts.h>
#import "RXTwitterController.h"
#import "TWAPIManager.h"


#define ERROR_TITLE_MSG @"Whoa, there cowboy"
#define ERROR_NO_ACCOUNTS @"You must add a Twitter account in Settings.app to use this demo."
#define ERROR_PERM_ACCESS @"We weren't granted access to the user's accounts"
#define ERROR_NO_KEYS @"You need to add your Twitter app keys to Info.plist to use this demo.\nPlease see README.md for more info."
#define ERROR_OK @"OK"



@interface RXTwitterController ()

@property (nonatomic, strong) TWAPIManager *apiManager;
@property (nonatomic, strong) NSArray *accounts;
@property (nonatomic, strong) ACAccountStore *accountStore;

@end


@implementation RXTwitterController
@synthesize delegate = _delegate;

-(id)init
{
  self = [super init];
  if(self)
  {
    _accountStore = [[ACAccountStore alloc]init];
    _apiManager = [[TWAPIManager alloc] init];
  }
  
  return self;
}

-(id)initWithDelegate:(id<RXTwitterDelegate>)delegate
{
  self = [self init];
  if(self)
  {
    _delegate = delegate;
  }
  return self;
}


-(void)connectWithTwitterWithView:(UIView *)view
{
  [self refreshTwitterAccounts];
  
  UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose an Account" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
  for (ACAccount *acct in _accounts) {
    [sheet addButtonWithTitle:acct.username];
  }
  sheet.cancelButtonIndex = [sheet addButtonWithTitle:@"Cancel"];
  [sheet showInView:view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if (buttonIndex != actionSheet.cancelButtonIndex) {
    [_apiManager performReverseAuthForAccount:_accounts[buttonIndex] withHandler:^(NSData *responseData, NSError *error) {
      if (responseData) {
        NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        TWDLog(@"Reverse Auth process returned: %@", responseStr);
        
        NSArray *parts = [responseStr componentsSeparatedByString:@"&"];
//        NSString *lined = [parts componentsJoinedByString:@"\n"];
        if (_delegate) {
          [_delegate loggedIn:parts];
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:lined delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//          [alert show];
//        });
      }
      else {
        TWALog(@"Reverse Auth process failed. Error returned was: %@\n", [error localizedDescription]);
      }
    }];
  }
}

- (void)refreshTwitterAccounts
{
  TWDLog(@"Refreshing Twitter Accounts \n");
  
  if (![TWAPIManager hasAppKeys]) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_NO_KEYS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
    [alert show];
  }
  else if (![TWAPIManager isLocalTwitterAccountAvailable]) {

  }
  else {
    [self obtainAccessToAccountsWithBlock:^(BOOL granted) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if (granted) {
//          _reverseAuthBtn.enabled = YES;
        }
        else {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ERROR_TITLE_MSG message:ERROR_PERM_ACCESS delegate:nil cancelButtonTitle:ERROR_OK otherButtonTitles:nil];
          [alert show];
          TWALog(@"You were not granted access to the Twitter accounts.");
        }
      });
    }];
  }
}

- (void)obtainAccessToAccountsWithBlock:(void (^)(BOOL))block
{
  ACAccountType *twitterType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
  
  ACAccountStoreRequestAccessCompletionHandler handler = ^(BOOL granted, NSError *error) {
    if (granted) {
      self.accounts = [_accountStore accountsWithAccountType:twitterType];
    }
    
    block(granted);
  };
  
  //  This method changed in iOS6. If the new version isn't available, fall back to the original (which means that we're running on iOS5+).
  if ([_accountStore respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
    [_accountStore requestAccessToAccountsWithType:twitterType options:nil completion:handler];
  }
  else {
    [_accountStore requestAccessToAccountsWithType:twitterType withCompletionHandler:handler];
  }
}
@end
